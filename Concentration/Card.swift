//
//  Card.swift
//  Concentration
//
//  Created by Prince on 2018-08-11.
//  Copyright © 2018 Prince Nimoh. All rights reserved.
//

import Foundation
import Darwin

internal struct Card {
    
    var isFaceUp = false
    var isMatched = false
    var identifier: Int
    private static var identifierFactory = 0
    
    
    
    private static func getUniqueIdentifier() -> Int
    {
        identifierFactory += 1
        return identifierFactory
    }
    
    init()
    {
        self.identifier = Card.getUniqueIdentifier()
    }
    
    //Returns an array of shuffled cards
    static func shuffleDeck(deckOfCards: [Card]) ->[Card]
    {
        var shuffledDeck = deckOfCards //Make deck of cards mutable
                                        //There must be a better way
        var tempCard: Card
        var randomlyPickedIndex: Int
        let numberOfTimesToShuffule = shuffledDeck.count * 3;
        
        for _ in 0..<numberOfTimesToShuffule
        {
            randomlyPickedIndex = shuffledDeck.count.arc4random
            
            tempCard = shuffledDeck.removeLast()
            shuffledDeck.insert(tempCard, at: randomlyPickedIndex)
        }
        
        return shuffledDeck
    }
}
