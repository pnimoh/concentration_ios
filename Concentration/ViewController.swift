//
//  ViewController.swift
//  Concentration
//
//  Created by Prince on 2018-08-09.
//  Copyright © 2018 Prince Nimoh. All rights reserved.
//

import UIKit

class ViewController: UIViewController
{
    private lazy var game = Concentration (numberOfPairsOfCards: numberOfPairsOfCards)
    
    var numberOfPairsOfCards: Int
    {
        return (CardButtons.count + 1)/2
    }
    
    private(set) var flipCount = 0 {
        didSet {
            flipCountLabel.text = "Flips: \(flipCount)"
        }
    }
    
    @IBOutlet private var CardButtons: [UIButton]!
    @IBOutlet private weak var flipCountLabel: UILabel!
    @IBAction private func touchCard(_ sender: UIButton)
    {        
        flipCount += 1
        
        if let cardNumber = CardButtons.index(of: sender)
        {
            //print("cardNumber = \(cardNumber)")
            game.chooseCard(at: cardNumber)
            updateViewFromModel()
        }
        
    }
    
    
    private func updateViewFromModel()
    {
        for index in CardButtons.indices
        {
            let button = CardButtons[index]
            let card = game.cards[index]
            if card.isFaceUp
            {
                button.setTitle(emoji(for: card), for: UIControlState.normal)
                button.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            }
            else
            {
                button.setTitle("", for: UIControlState.normal)
                button.backgroundColor = card.isMatched ? #colorLiteral(red: 1, green: 0.5763723254, blue: 0, alpha: 0) : #colorLiteral(red: 1, green: 0.5763723254, blue: 0, alpha: 1)
            }
        }
    }
    
    private var emojiChoices = ["🦇", "😱","🙀","😈","🎃", "👻","🍭", "🍬", "🍎"]
    
    private var emoji = [Int: String]()
    
    private func emoji(for card: Card) -> String
    {
        if emoji[card.identifier] == nil, emojiChoices.count > 0
        {
            emoji[card.identifier] = emojiChoices.remove(at: emojiChoices.count.arc4random)
        }
        
        return emoji[card.identifier] ?? "?"
    }
}

/*Extension of the type Int to support generating a random number
 */
extension Int
{
    var arc4random: Int
    {
        if self > 0
        {
            return Int(arc4random_uniform(UInt32(self))) //return a random Int between zero and self
        }
        else if self < 0
        {
            return -Int(arc4random_uniform(UInt32(abs(self)))) //return a random Int between self and zero
        }
        else
        {
            return 0
        }
    }
}
